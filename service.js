var express = require('express');
var bodyparser = require('body-parser');
var expressjwt = require('express-jwt');
var cors = require('cors');

var service = express();
service.use(bodyparser.urlencoded({ extended: true}));
service.use(bodyparser.json());
service.use(cors());
service.use(expressjwt({secret:'token_secret'}).unless({path:['/auth/login']}));

var connection = require('./conn');
var routes = require('./routes');

connection.initialize();
routes.configurate(service);

var server = service.listen(8080, function(){
    console.log('Listen in port ', server.address().port);
})