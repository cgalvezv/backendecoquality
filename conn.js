var mysql = require('mysql');

function Connection(){
    this.pool = null;

    this.initialize = function()
    {
        this.pool = mysql.createPool({
            connectionLimit: 10,
            host: 'localhost',
            user: 'root',
            password: '12345',
            database: 'database_ecoquality'
        })
    }

    this.getConn = function(callback){
        this.pool.getConnection(function(error, connection){
            callback(error,connection);
        })
    }
}

module.exports = new Connection();