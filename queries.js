var conn = require('./conn');
var nodemailer = require('nodemailer');
var jwt = require('jsonwebtoken');
var fs = require('fs');
var pdf = require('html-pdf');
var createHTML = require('create-html');
var bcrypt = require('bcrypt-nodejs');

function queriesDB()
{
    this.login = function(data, response) {
        conn.getConn(function(er,cn){                        
            cn.query('select * from users where rut_person=? and id_user_role = 3',[data.rut], function(error, result){
                cn.release();
                if(error)
                {
                    response.send('error');
                }
                else
                {
                    if (result.length == 0) 
                    {                        
                        console.log('No se encuentra el usuario.');
                        response.send('noFound');
                    }
                    else
                    {
                        var password = result[0].password;
                        var finalPass = password.replace('$2y$', '$2a$');                           
                        bcrypt.compare(data.password, finalPass, function(err, res) {
                            if(res)
                            {
                                var token = jwt.sign({
                                    user: data.rut,
                                    rol: 'admin'
                                },'token_secret',{expiresIn: '20h'});
                                response.send(token);
                            }
                            else
                            {
                                console.log('No se encuentra el usuario.');
                                response.send('noFound');
                            }
                        });                                                                                                       
                    }
                }
            })
        })
    }

    this.selectMonitorPerRut = function(rut, response){
        conn.getConn(function(er,cn){
            cn.query('select u.username, u.email, p.rut, p.name, p.lastname, p.address, p.phone_number from users u inner join persons p on u.rut_person = p.rut where u.id_user_role = 3 and u.rut_person = ?', rut, function(error, result){
                cn.release();
                if(error)
                {
                    response.send({ estado: 'Error' })
                }
                else
                {
                    response.send(result);
                }
            })
        })
    }

    this.selectClientPerDay = function(rutUser, date, response){
        conn.getConn(function(er,cn){
            cn.query('select distinct c.id, c.name, c.physical_place, c.email from clients c inner join control_templates ct on c.id = ct.id_client inner join scheduled_visits sv on ct.id = sv.id_control_template inner join visits_users vu on sv.id = vu.id_visit where  vu.rut_user = ? and date(sv.visit_date) = ?',[rutUser, date],function(error, result){                
                cn.release();
                if(error)
                {
                    response.send({ estado: 'Error' })
                }
                else
                {                    
                    response.send(result);
                }
            })
        })
    }

    this.selectTrapTypePerVisitDate = function(date, id_client, response){
        conn.getConn(function(er,cn){
            cn.query('select distinct ctt.id, ctt.name template_name, tt.name trap_type_name from control_template_types ctt inner join trap_types tt on ctt.id = tt.id inner join control_templates ct on ctt.id = ct.id_control_template_type inner join scheduled_visits sv on sv.id_control_template = ct.id where date(sv.visit_date) = ? and ct.id_client = ? and sv.isVisited = 0',[date, id_client],function(error, result){
                cn.release();
                if(error)
                {
                    response.send({ estado: 'Error' })
                }
                else
                {
                    response.send(result);
                }
            })
        })
    }

    this.selectControlTemplate = function(idClient, idTrapType, response){
        conn.getConn(function(er,cn){
            cn.query('select ct.id, sv.id id_visit, ct.id_client, ct.id_control_template_type, ct.total_trap, sv.visit_date from control_templates ct inner join scheduled_visits sv on ct.id = sv.id_control_template where ct.id_client = ? and ct.id_control_template_type = ?', [idClient, idTrapType], function(error, result){
                cn.release();
                if(error)
                {
                    response.send('error');
                }
                else
                {
                    response.send(result);
                }
            })
        })
    }

    this.selectAllStateRegister = function(idControlTemplate, response){
        conn.getConn(function(er,cn){
            cn.query('select sr.id, sr.id_trap, sr.id_trap_state, sr.is_registered, t.id_trap_location, t.trap_number, t.trap_letter from state_records sr inner join traps t on sr.id_trap = t.id inner join control_templates ct on ct.id_client = t.id_client and ct.id_control_template_type = t.id_trap_type where ct.id = ? order by t.trap_number asc, t.trap_letter asc',idControlTemplate ,function(error, result){
                cn.release();
                if(error)
                {
                    response.send('error');
                }
                else
                {
                    response.send(result);
                }
            })
        })
    }

    this.selectAllStateTrap = function(idTrapType, response){
        conn.getConn(function(er,cn){
            if (idTrapType == 1) {
                cn.query('select * from trap_states st where st.id >= 1 and st.id <= 5', function(error, result){
                    cn.release();
                    if(error)
                    {
                        response.send('error');
                    }
                    else
                    {
                        response.send(result);
                    }
                })
            } else {
                cn.query('select * from trap_states st where st.id >= 6 and st.id <= 9', function(error, result){
                    cn.release();
                    if(error)
                    {
                        response.send('error');
                    }
                    else
                    {
                        response.send(result);
                    }
                })
            }
        })
    }    
    

    this.selectRutForUsername = function(username, response){
        conn.getConn(function(er,cn){
            cn.query('select u.rut_person from user u where u.username = ?', username, function(error, result){
                cn.release();
                if(error)
                {
                    response.send({ estado: 'Error' })
                }
                else
                {
                    response.send(result);
                }
            })
        })
    }
    

    this.updateStateRegister = function(data, response){
        conn.getConn(function(er,cn){
            cn.query('update state_records set `id_trap_state` = ? , `is_registered` = ? where id = ?',[data.id_trap_state, data.is_registered, data.id],function(error, result){
                cn.release();
                if(error)
                {
                    response.send('error');
                }
                else
                {
                    response.send(result);
                }
            })
        })
    }

    this.updateVisit = function(data, response){
        conn.getConn(function(er,cn){            
            cn.query('update scheduled_visits set `findings` = ? , `actions` = ? , `isVisited` = 1 where id_control_template = ?',[data.findings, data.actions, data.id_control_template],function(error, result){                
                cn.release();
                if(error)
                {
                    response.send('error');
                }
                else
                {
                    response.send(result);
                }
            })
        })
    }

    this.sendMailClient = function(data, response) {

        var html = createHTML({
          title: 'example',
          css: 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
          body: data.body+'<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>',
        });

        fs.writeFile('index.html',html , function (err) {
            if (err) console.log(err)
            var html = fs.readFileSync('./index.html', 'utf8');
            var options = { format: 'A4' };

            pdf.create(html, options).toFile('./doc-control.pdf', function(err, res) {
                if (err) return console.log(err);
                var transporter = nodemailer.createTransport({
                    service: 'gmail',
                    port: 25,
                    secure: false, // secure:true for port 465, secure:false for port 587
                    auth: {
                        user: 'camilogalvezv@gmail.com',
                        pass: 'tito77guox-k'
                    },
                    tls:{
                        rejectUnauthorized: false
                    }
                });

                var mailOptions = {
                    from: '"Camilo Gálvez" <camilogalvezv@gmail.com>', // sender address
                    to: data.mailClient, // list of receivers
                    subject: data.subject, // Subject line
                    html: "adjunto doc", // html body
                    attachments: [{
                        path: './doc-control.pdf'
                    }]
                };

                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        return console.log(error);
                    }
                    console.log('Mail Sended!!!');
                    fs.unlink('./doc-control.pdf',function(err){
                        if(err) return console.log(err);
                        console.log('file deleted successfully');
                    });                 

                    fs.unlink('./index.html',function(err){
                        if(err) return console.log(err);
                        console.log('file deleted successfully');
                    });
                });
            });
        });        
    }
}
module.exports = new queriesDB();