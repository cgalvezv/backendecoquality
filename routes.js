var db = require('./queries');

function http()
{
    this.configurate = function(service) {

        //Login 
        service.post('/auth/login', function(request, response){
            db.login(request.body,response);
        })
        //Clients per day
        service.get('/client/:rut/:date/', function(request, response) {
            db.selectClientPerDay(request.params.rut,request.params.date,response);            
        })
        //Monitor Info
        service.get('/monitor/:rut/', function(request, response) {
            db.selectMonitorPerRut(request.params.rut, response);
        })        
        //TrapType Selection
        service.get('/trapType/:date/:idClient/', function(request, response) {
            db.selectTrapTypePerVisitDate(request.params.date,request.params.idClient,response);                    
        })
        //Detail Control Template
        service.get('/controlTemplate/:idClient/:idTrapType/', function(request, response) {
            db.selectControlTemplate(request.params.idClient,request.params.idTrapType, response);
        })
        //All State Records
        service.get('/stateRegister/:idControlTemplate/', function(request, response) {
            db.selectAllStateRegister(request.params.idControlTemplate, response);
        })        

        service.get('/stateTrap/:idTrapType/', function(request, response) {
            db.selectAllStateTrap(request.params.idTrapType, response);
        })        

        service.put('/stateRegister/', function(request, response){
            db.updateStateRegister(request.body, response);
        })

        service.put('/visit/', function(request, response){
            db.updateVisit(request.body, response);
        })

        service.post('/controlTemplate/sendMail/', function(request, response) {
            db.sendMailClient(request.body, response);
        })      
    }
}

module.exports = new http();